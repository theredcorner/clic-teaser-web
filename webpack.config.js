const path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const LiveReloadPlugin = require('webpack-livereload-plugin');

module.exports = {
  entry: './src/index.js',
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.(scss|css)$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
          },
          {
            loader: 'css-loader',
            options: {
              modules: false,
            },
          },
          {
            loader: 'sass-loader',
            options: {
              sassOptions: {
                includePaths: [path.resolve(__dirname, 'node_modules')],
              },
            },
          },
          {
            loader: 'import-glob-loader',
            options: {},
          },
        ],
      },
      {
        test: /\.(png|jpg|jpeg|svg)$/,
        use: {
          loader: 'file-loader',
          options: {
            outputPath: '/images',
            publicPath: '/dist/images',
          },
        },
      },
      {
        test: /\.(woff|woff2)$/,
        use: {
          loader: 'file-loader',
          options: {
            outputPath: '/fonts',
            publicPath: '/dist/fonts',
          },
        },
      },
    ],
  },
  output: {
    path: path.resolve('./dist'),
    filename: 'app.js',
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: '[name].css',
      chunkFilename: '[id].css',
    }),
    require('autoprefixer'),
    new LiveReloadPlugin(),
  ],
};
