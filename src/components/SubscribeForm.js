import React, { Component, useState } from 'react';
import { motion } from 'framer-motion';
import Logo from './Logo';

// <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[3]='MMERGE3';ftypes[3]='dropdown';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[4]='MMERGE4';ftypes[4]='text';fnames[0]='EMAIL';ftypes[0]='email';}(jQuery));var $mcj = jQuery.noConflict(true);</script>

const DisclosureArrow = (props) => {
  const { fill } = props;
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="8.277" height="13.243" viewBox="0 0 8.277 13.243">
      <path id="arrow" d="M5,1.655,6.687,0l6.59,6.621-6.59,6.621L5,11.587,9.966,6.621Z" transform="translate(-5)" fill={fill || '#FFF'} />
    </svg>
  );
};

const RawMailchimp = () => {
  const [gender, setGender] = useState(null);

  return (
    <>
      <div id="mc_embed_signup">
        <form
          action="https://theredcorner.us4.list-manage.com/subscribe/post?u=c64218f044d610de772689e0c&amp;id=669b9dc88d"
          method="post"
          id="mc-embedded-subscribe-form"
          name="mc-embedded-subscribe-form"
          className="validate"
          target="_blank"
          onSubmit={(event) => {
            if (gender == null || gender === 'Select gender') {
              alert('Validation error: Please select a gender.');
              event.preventDefault();
            }
          }}
        >
          <div id="mc_embed_signup_scroll">

        <div className="columns is-multiline">
          <motion.div
              animate={{
                y: [10, 0],
                opacity: [0, 1],
              }}
              transition={{
                default: { duration: 0.3 },
                delay: 0.3,
              }}
              className="mc-field-group column is-2-desktop is-6"
          >
            <label for="mce-MMERGE3">Gender <span>Required</span></label>

            <div className="form__dropdown">
              <DisclosureArrow />
              <select
                name="MMERGE3"
                className="required"
                required
                id="mce-MMERGE3"
                onChange={(event) => setGender(event.target.value)}
                value={gender}
              >
                <option>Select gender</option>
                <option value="Male">Male</option>
                <option value="Female">Female</option>
                <option value="Other">Other</option>
                <option value="Prefer not to say">Prefer not to say</option>
              </select>
            </div>
          </motion.div>

          <motion.div
            animate={{
              y: [10, 0],
              opacity: [0, 1],
            }}
            transition={{
              default: { duration: 0.3 },
              delay: 0.3,
            }}
            className="mc-field-group column is-2-desktop is-6"
          >
            <label for="mce-FNAME">First Name <span>Required</span></label>
            <input type="text" name="FNAME" className="required" required id="mce-FNAME" />
          </motion.div>

          <motion.div
            animate={{
              y: [10, 0],
              opacity: [0, 1],
            }}
            transition={{
              default: { duration: 0.3 },
              delay: 0.4,
            }}
            className="mc-field-group column is-2-desktop is-6"
          >
            <label for="mce-LNAME">Last Name <span>Required</span></label>
            <input type="text" name="LNAME" className="required" required id="mce-LNAME" />
          </motion.div>

          <motion.div
            animate={{
              y: [10, 0],
              opacity: [0, 1],
            }}
            transition={{
              default: { duration: 0.3 },
              delay: 0.5,
            }}
            className="mc-field-group column is-2-desktop is-6"
          >
            <label for="mce-MMERGE4">Company name <span>Required</span></label>
            <input type="text" name="MMERGE4" className="required" required id="mce-MMERGE4" />
          </motion.div>

          <motion.div
            animate={{
              y: [10, 0],
              opacity: [0, 1],
            }}
            transition={{
              default: { duration: 0.3 },
              delay: 0.6,
            }}
            className="mc-field-group column is-2-desktop is-6"
          >
            <label for="mce-EMAIL">Email Address  <span>Required</span>
          </label>
            <input type="email" name="EMAIL" className="required email" required id="mce-EMAIL" />
          </motion.div>
            <div id="mce-responses" className="clear">
              <div className="response" id="mce-error-response" style={{ display: 'none' }}></div>
              <div className="response" id="mce-success-response" style={{ display: 'none' }}></div>
            </div>
              <div style={{ position: 'absolute', left: -5000 }} aria-hidden="true"><input type="text" name="b_c64218f044d610de772689e0c_669b9dc88d" tabindex="-1" /></div>
              <div className="clear column is-2-desktop is-6"><input type="submit" value="Stay informed" name="subscribe" id="mc-embedded-subscribe" className="button" /></div>
            </div>
          </div>
        </form>
      </div>
    </>
  );
};

export default (props) => {
  return (
    <motion.div
      animate={{
        y: ['100%', 0],
      }}
      transition={{
        default: { duration: 0.4 },
      }}
      className="bottom-modal"
    >
      <div className="container">
        <p>
          Leave your e-mail here and get to know all about CLIC
        </p>

        <RawMailchimp />
      </div>
    </motion.div>
  );
};
