import React, { Component } from 'react';
import ClicLogo from '../assets/images/clic-logo.svg';

export default (props) => {
  const { className } = props;

  return <img src={ClicLogo} alt="CLIC" className={className} />;
};
