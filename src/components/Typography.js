import React, { Component } from 'react';

export const HighlightedText = (props) => {
  const { children } = props;

  return <span className="has-text-yellow">{children}</span>;
};

export const ExtraLightText = (props) => {
  const { children } = props;

  return <span className="has-text-extra-light">{children}</span>;
};
