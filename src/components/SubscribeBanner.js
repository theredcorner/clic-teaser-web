import React, { Component, useState } from 'react';

const DisclosureArrow = (props) => {
  const { fill } = props;
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="8.277" height="13.243" viewBox="0 0 8.277 13.243">
      <path id="arrow" d="M5,1.655,6.687,0l6.59,6.621-6.59,6.621L5,11.587,9.966,6.621Z" transform="translate(-5)" fill={fill || '#FFF'} />
    </svg>
  );
};

const LineButton = ({ title, onClick }) => {
  return (
    // eslint-disable-next-line jsx-a11y/anchor-is-valid
    <a href="#" className="line-button" onClick={onClick}>
      {title}
      <DisclosureArrow />
    </a>
  );
};

export default (props) => {
  const [isVisible, setIsVisible] = useState(false);

  const onClick = (event) => {
    event.preventDefault();
    setIsVisible(!isVisible);
  };

  return (
    <>
      {isVisible && (
        props.onShow()
      )}

      {isVisible !== true && (
        <div className="subscribe-banner">
          <div className="container">
            <p>
              Leave your e-mail here and get to know all about CLIC
            </p>

            <LineButton title="Subscribe now" onClick={onClick} />
          </div>
        </div>
      )}
    </>
  );
};
