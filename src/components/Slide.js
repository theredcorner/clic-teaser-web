import React, { Component } from 'react';
import { motion } from 'framer-motion';

export default (props) => {
  const { children, backgroundImage, backgroundComponent } = props;

  return (
    <div className="slide" style={{ backgroundImage: `url(${backgroundImage})` }}>
      {backgroundComponent}
      <div className="slide__oval-gradient" />

      <div className="slide__inner">
        <div className="slide__children-container">
          <div className="slide__children">
            {children}
          </div>
        </div>
      </div>
    </div>
  );
};

export const SlideDescription = (props) => {
  const { children, delay } = props;

  return (
    <div className="animations__appear-mask">
      <motion.div
        animate={{
          opacity: [0, 1],
          y: ['100%', 0],
        }}
        transition={{
          delay: delay || 6.7,
          duration: 0.6,
          ease: 'easeout',
        }}
      >
        <h4 style={{ maxWidth: 700, lineHeight: 1.3 }}>{children}</h4>
      </motion.div>
    </div>
  );
};


export const SlideTitle = (props) => {
  const { children, delay } = props;

  return (
    <div className="animations__appear-mask">
      <motion.div
        animate={{
          opacity: [0, 1],
          y: ['100%', 0],
        }}
        transition={{
          delay: delay || 6.2,
          duration: 0.3,
          ease: 'easeout',
        }}
      >
        <h2>{children}</h2>
      </motion.div>
    </div>
  );
};
