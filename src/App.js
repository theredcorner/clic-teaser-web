import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from 'react-router-dom';

import Home from './containers/Home';
import Slide, { SlideTitle, SlideDescription } from './components/Slide';

import Slide3Image from './assets/images/slides/slide3-talent.jpg';
import Logo from './components/Logo';
import { motion } from 'framer-motion';

const ThankYou = () => (
  <>
    <a href="/">
      <Logo className="slide__logo" />
    </a>

    <Slide backgroundImage={Slide3Image}>
      <SlideTitle delay={1}>
        Almost done! Please click the link in the email we sent you.
      </SlideTitle>

      <SlideDescription delay={1.5}>
        We will keep you posted on updates, we will share our knowledge with you and most importantly let you know when our spaces become available.

        <br /><br />
      </SlideDescription>

      <motion.div
        animate={{
          opacity: [0, 1],
          y: ['100%', 0],
        }}
        transition={{
          delay: 1.8,
          duration: 0.6,
          ease: 'easeout',
        }}
      >
        <a href="/" className="line-button">Back to homepage</a>
      </motion.div>
    </Slide>
  </>
);

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/thank-you" exact>
          <ThankYou />
        </Route>

        <Route path="/">
          <Home />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
