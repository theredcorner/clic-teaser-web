/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable max-classes-per-file */
import React, { Component, Fragment } from 'react';
import { motion } from 'framer-motion';

import Slide, { SlideTitle, SlideDescription } from '../components/Slide';
import { HighlightedText, ExtraLightText } from '../components/Typography';

import Slide2Image from '../assets/images/slides/slide2-ecosystem.jpg';
import Slide3Image from '../assets/images/slides/slide3-talent.jpg';
import Slide4Image from '../assets/images/slides/slide4-estate.png';
import Slide5Image from '../assets/images/slides/slide5-energy.jpg';

import SubscribeBanner from '../components/SubscribeBanner';
import SubscribeForm from '../components/SubscribeForm';

import ReactMapboxGl, { Layer, Feature, Marker } from 'react-mapbox-gl';
import Logo from '../components/Logo';


const Timeline = {
  duration: 10000,
  tick: 750,

  toMotionValue(value) {
    return Math.round(value / 1000);
  },
};

class MotionTick extends Component {
  constructor(props) {
    super(props);

    this.state = {
      progress: 0.0,
      roundsCount: 0,
      isPaused: false,
    };

    this.tick = this.tick.bind(this);
    this.pause = this.pause.bind(this);
    this.scheduleTick = this.scheduleTick.bind(this);
  }

  componentDidMount() {
    const { initialDelay } = this.props;
    setTimeout(this.scheduleTick, initialDelay || 0);

    window.addEventListener('click', this.pause);
  }

  componentWillUnmount() {
    window.removeEventListener('click', this.pause);
  }

  scheduleTick() {
    this.tickInterval = setInterval(this.tick, Timeline.tick);
  }

  componentWillUnmount() {
    clearInterval(this.tickInterval);
  }

  get rounds() {
    return this.state.roundsCount - (Math.floor(this.state.roundsCount / this.props.rounds) * this.props.rounds);
  }

  tick() {
    if (this.state.progress > 0.95) {
      this.setState((prevState) => ({
        progress: 0.0,
        roundsCount: prevState.roundsCount + 1,
      }));
    }

    this.setState((prevState) => ({
      progress: Math.round((prevState.progress + 0.05) * 100) / 100,
    }));
  }

  pause(event) {
    const { isPaused } = this.state;

    if (isPaused === true) {
      this.scheduleTick();
      this.setState({ isPaused: false });

      if (typeof this.props.onPlay === 'function') { this.props.onPlay(); }
      return;
    }

    clearInterval(this.tickInterval);
    this.setState({ isPaused: true });

    if (typeof this.props.onPause === 'function') { this.props.onPause(); }
  }

  render() {
    const { progress, isPaused, roundsCount } = this.state;

    return typeof this.props.onRender === 'function'
      ? this.props.onRender(this.rounds, progress, {
        roundsCount,
        isPaused,
      }) : null;
  }
}

class Slider extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isPaused: false,
    };
  }

  // eslint-disable-next-line class-methods-use-this
  renderSlidingDoors(progress, { roundsCount, isPaused }) {
    return (
      <>
        {progress > 0.7 && (
          <motion.div
            className="slider__sliding-door"
            animate={{ opacity: [0, 1.0] }}
            transition={{
              default: { duration: 0.8 },
            }}
          />
        )}

        {progress < 0.5 && (
          <>
            <motion.div
              className="slider__sliding-door--left"
              animate={{ x: [0, '-100%'] }}
              transition={{
                delay: roundsCount === 0 ? 3.5 : 1,
                default: { duration: 3 },
              }}
            />

            <motion.div
              className="slider__sliding-door--right"
              animate={{ x: [0, '100%'] }}
              transition={{
                delay: roundsCount === 0 ? 3.5 : 1,
                default: { duration: 3 },
              }}
            />
          </>
        )}

        {isPaused !== true && (
          <>
            {roundsCount === 0 && progress < 0.85 && (
              <motion.div
                className="bottom-yellow-line"
                animate={{ width: [0, '100%'] }}
                transition={{
                  default: {
                    duration: Math.round((750 * 16) / 1000),
                  },
                }}
              />
            )}

            {(progress > 0.85 || (roundsCount > 0 && progress < 0.85)) && (
              <motion.div
                className="bottom-yellow-line"
                animate={{ width: [0, '100%'] }}
                transition={{
                  default: {
                    duration: Math.round((750 * 16.5) / 1000),
                  },
                }}
              />
            )}

            {progress === 0.85 && (
              <motion.div
                animate={{ opacity: [1, 0] }}
                transition={{
                  duration: 0.5,
                }}
                className="bottom-yellow-line"
              />
            )}
          </>
        )}
      </>
    );
  }

  // eslint-disable-next-line class-methods-use-this
  renderSlide(slide, nextSlide, progress, { roundsCount }) {
    const { quote } = slide.props;
    const nextQuote = nextSlide.props.quote;

    return (
      <>
        {slide}

        {(progress < 0.5) && (
          <motion.div
            className="slider__quote"
            animate={{ opacity: [1.0, 0.0] }}
            transition={{
              delay: roundsCount === 0 ? 3.5 : 1,
              default: { duration: 0.8 },
            }}
          >
            {quote}
          </motion.div>
        )}

        {(progress > 0.8) && (
          <motion.div
            className="slider__quote"
            animate={{ opacity: [0.0, 1.0] }}
            transition={{
              delay: 0,
              default: { duration: 0.8 },
            }}
          >
            {nextQuote}
          </motion.div>
        )}
      </>
    );
  }

  render() {
    const { children } = this.props;
    const { isPaused } = this.state;

    return (
      <div className={['slider', isPaused ? 'slider--is-paused' : ''].join(' ')}>
        <a className="slider__pause-button" href="#"></a>

        <MotionTick
          rounds={children.length}
          onPause={() => {
            this.setState({ isPaused: true });
          }}
          onPlay={() => {
            this.setState({ isPaused: false });
          }}
          onRender={(round, progress, additionals) => {
            const slide = children[round];
            const nextSlide = round + 1 < children.length ? children[round + 1] : children[0];

            return (
              <>
                {this.renderSlidingDoors(progress, additionals)}
                {this.renderSlide(slide, nextSlide, progress, additionals)}
              </>
            );
          }}
        />
      </div>
    );
  }
}

const Map = ReactMapboxGl({
  accessToken:
    'pk.eyJ1IjoiZmlnaHRjbHViZGlnaXRhbCIsImEiOiJjazY3eWNiMGcxdXd1M2Zxd291eG9hd2I1In0.rCL67L_zPjcLFeCIZsdPkw'
});

class ZoomingMap extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      zoom: 8,
    };
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({ zoom: 11 });
    }, 6000);
  }

  render() {
    return (
      <Map
        center={[4.75930095, 52.34526334]}
        zoom={[this.state.zoom]}
        containerStyle={{
          width: '140%',
          height: '100%',
          position: 'absolute',
          left: 0,
          top: 0,
          zIndex: 5,
        }}
        movingMethod="flyTo"
        style="mapbox://styles/mapbox/streets-v9"
        flyToOptions={{ duration: 3000 }}
      >
        <Marker
          coordinates={[4.75930095, 52.34526334]}
          anchor="bottom"
        >
          <img src={"https://i.imgur.com/MK4NUzI.png"} />
        </Marker>
      </Map>
    );
  }
}

export default () => (
  <>
    <a href="/">
      <Logo className="slide__logo" />
    </a>

    <Slider>
      <Slide
        backgroundComponent={(
          <ZoomingMap />
        )}
        quote={(
          <blockquote key="quote" className="blockquote--medium">
            I could either watch it happen or be a part of it
            <cite>Elon Musk</cite>
          </blockquote>
        )}
      >
        <SlideTitle>
          <HighlightedText>CLIC</HighlightedText> is situated right in the heart of the logistics corridor of the Amsterdam metropolitan area;
        </SlideTitle>

        <SlideDescription>
          Directly connected to the A9 motorway,
          facing Schiphol Airport and only 6 miles from the city center
        </SlideDescription>
      </Slide>


      <Slide
        backgroundImage={Slide2Image}
        quote={(
          <blockquote key="quote" className="blockquote--medium">
            The best way to predict the future, is to create it
            <cite>
              Peter Drucker, management thinker
            </cite>
          </blockquote>
        )}
      >

        <SlideTitle>
          <HighlightedText>CLIC</HighlightedText> is all about co-creation,
        </SlideTitle>

        <SlideDescription>
          Here food-, non-food- and express companies are innovating together with automotive-, energy-, ICT- and logistics companies.
        </SlideDescription>
      </Slide>

      <Slide
        backgroundImage={Slide3Image}
        quote={(
          <blockquote key="quote" className="blockquote--small">
            No matter who you are, most of the smartest people work for someone else
            <cite>Joy, co-founder Sun Microsystems</cite>
          </blockquote>
        )}
      >
        <SlideTitle>
          <HighlightedText>CLIC</HighlightedText> is where all kinds of talent join forces
          to come up with groundbreaking city logistic solutions
        </SlideTitle>

        <SlideDescription>
          From ideas with impact, to proof-of-concept solutions
          to fully operational logistics
        </SlideDescription>
      </Slide>

      <Slide
        backgroundImage={Slide4Image}
        quote={(
          <blockquote key="quote" className="blockquote--small">
            You have to believe that the dots will somehow connect in your future
            <cite>Jobs, founder Apple</cite>
          </blockquote>
        )}
      >
        <SlideTitle>
          <HighlightedText>CLIC</HighlightedText> offers a variety of spaces in a sustainable environment
        </SlideTitle>

        <SlideDescription>
          120,000m2 of offices, business spaces and logistic hubs, which can be fully tailored 
          to each company's individual needs and shared services, such as energy supply, park management and hotel facilities
        </SlideDescription>
      </Slide>

      <Slide
        backgroundImage={Slide5Image}
        quote={(
          <blockquote key="quote" className="blockquote--medium">
            To invent, you need a good imagination and a pile of junk
            <cite>Edison, founder General Electric Company</cite>
          </blockquote>
        )}
      >
        <SlideTitle>
          <HighlightedText>CLIC</HighlightedText> enables more than 50,000,000 zero-emission km's, thanks to its own sustainable energy-smartgrid
        </SlideTitle>

        <SlideDescription>
          Providing all the green power you need
          to send an entire electric fleet into the zero-emission areas of the city
        </SlideDescription>
      </Slide>
    </Slider>

    <SubscribeBanner
      onShow={() => (
        <SubscribeForm />
      )}
    />
  </>
);
